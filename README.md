# Дипломная работа по курсу "DevOps-инженер"
![header](./assets/images/header.png)
---
## Содержание  
- [1. Цели дипломной работы](#objectives)  
- [2. Подготовительные действия](#pre-stage)  
- [3. Создание облачной инфраструктуры](#first-stage)  
- [4. Создание Kubernetes кластера](#second-stage)  
- [5. Создание тестового приложения](#third-stage)  
- [6. Подготовка системы мониторинга и деплой приложения](#fourth-stage)  
- [7. Установка и настройка CI/CD](#fifth-stage) 
- [8. Заключение](#conclusions) 

## <a name="objectives"></a> 1. Цели дипломной работы

1. Подготовить облачную инфраструктуру на базе облачного провайдера *Яндекс.Облако*.
2. Запустить и сконфигурировать *Kubernetes* кластер.
3. Установить и настроить систему мониторинга.
4. Настроить и автоматизировать сборку тестового приложения с использованием *Docker-контейнеров*.
5. Настроить *CI* для автоматической сборки и тестирования.
6. Настроить *CD* для автоматического развёртывания приложения.

## <a name="pre-stage"></a> 2. Подготовительные действия

До начала выполнения заданий необходимо было выбрать систему контроля версий и CI/CD-процессов.

В её качестве был выбран *GitLab* (публичная версия).

Выбор обусловлен тем, что у него удобный процесс создания папйплайнов, есть *Self-Hosted* версия (а такое может потребоваться), а также тем, что для всего проекта возможно создать единую группу.

## <a name="first-stage"></a> 3. Создание облачной инфраструктуры

### Задание

Для начала необходимо подготовить облачную инфраструктуру в *Яндекс.Облаке* при помощи [*Terraform*](https://www.terraform.io/).

Особенности выполнения:
- Бюджет купона ограничен, что следует иметь в виду при проектировании инфраструктуры и использовании ресурсов.

Для облачного *K8s* используйте региональный мастер (неотказоустойчивый). Для *Self-Hosted K8s* минимизируйте ресурсы ВМ и долю ЦПУ. В обоих вариантах используйте прерываемые ВМ для *Worker Nodes*.

Предварительная подготовка к установке и запуску *Kubernetes* кластера.
1. Создайте сервисный аккаунт, который будет в дальнейшем использоваться *Terraform* для работы с инфраструктурой с необходимыми и достаточными правами. Не стоит использовать права суперпользователя.
2. Подготовьте [*backend*](https://www.terraform.io/docs/language/settings/backends/index.html) для *Terraform*:  
   а. Рекомендуемый вариант: *S3 Bucket* в созданном *Яндекс.Облаке* аккаунте (создание бакета через *Terraform*)  
   б. Альтернативный вариант: [*Terraform Cloud*](https://app.terraform.io/)  
3. Создайте *VPC* с подсетями в разных зонах доступности.
4. Убедитесь, что теперь вы можете выполнить команды `terraform destroy` и `terraform apply` без дополнительных ручных действий.
5. В случае использования [*Terraform Cloud*](https://app.terraform.io/) в качестве [*backend*](https://www.terraform.io/docs/language/settings/backends/index.html) убедитесь, что применение изменений успешно проходит, используя WEB-интерфейс *Terraform Cloud*.

Ожидаемые результаты:
1. *Terraform* сконфигурирован и создание инфраструктуры посредством *Terraform* возможно без дополнительных ручных действий.
2. Полученная конфигурация инфраструктуры является предварительной, поэтому в ходе дальнейшего выполнения задания возможны изменения.

---

### Результат

В процессе планирования инфраструктуры было принято решение для *K8s-кластера* использовать 3 мастер и 2 воркер ноды, в качестве *backend* и интерфейса управления был выбран *Terraform Cloud*.

*Terraform Cloud* был выбран в следствии того, что его можно использовать сразу (на первом этапе выполнения работы). Но тут стоит подчернить, что у такого решения есть минусы, оно проприетарное и не может использоваться в закрытых контурах.

[Репозиторий с *Terraform-манифестами*](https://gitlab.com/netology-devops-project/terraform)

Подготовительные действия выполняются с помощью *Yandex Cloud CLI*.


1. Создание сервисного акканута для *Terraform*:
    ```bash
    [manokhin@local:~()]> yc iam service-account create --name sa-terraform
    done (1s)
    id: aje49qg3gk83********
    folder_id: b1gjd8bnpqdp********
    created_at: "2024-06-17T04:10:16.243697589Z"
    name: sa-terraform
    ```
2. Назначение роли *editor* сервисному аккаунту:
    ```bash
    [manokhin@local:~()]> yc resource-manager folder add-access-binding default --role editor --subject serviceAccount:aje49qg3gk83********
    done (3s)
    effective_deltas:
      - action: ADD
        access_binding:
          role_id: editor
          subject:
            id: aje49qg3gk83********
            type: serviceAccount
    ```
3. Создание ключа для сервисного аккаунта:
    ```bash
    [manokhin@local:~()]> yc iam key create --service-account-id aje49qg3gk83******** --folder-name default --output key.json
    id: ajelku453jlf********
    service_account_id: aje49qg3gk83********
    created_at: "2024-06-17T04:18:01.235727842Z"
    key_algorithm: RSA_2048
    ```
4. Создание и активация профиля:
    ```bash
    [manokhin@local:~()]> yc config profile create sa-terraform
    Profile 'sa-terraform' created and activated
    ```
5. Установка значений (*Ключ*, *ID-облака*, *ID-каталога*) для профиля:
    ```bash
    [manokhin@local:~()]> yc config set service-account-key key.json && \
    yc config set cloud-id b1gkueh3tmji******** && \
    yc config set folder-id b1gjd8bnpqdp********
    ```
6. Создание *access-key* и *secret*:
    ```bash
    [manokhin@local:~()]> yc iam access-key create --service-account-name sa-terraform
    access_key:
      id: ajevcnjltkv3********
      service_account_id: aje49qg3gk83********
      created_at: "2024-06-17T19:08:18.020780928Z"
      key_id: YCAJE3i_OdZVn0inD********
    secret: YCMHfDT84-R5LtcPRiTE4l6G****************
    ```

Описание Terraform-манифестов:
  1. [*main.tf*](https://gitlab.com/netology-devops-project/terraform/-/blob/main/main.tf?ref_type=heads) - декларирует ресурсы
  2. [*output.tf*](https://gitlab.com/netology-devops-project/terraform/-/blob/main/output.tf?ref_type=heads) - выводит информацию о *хостнейме*, *приватном* и *публичном* *IP-адресе* после применения конфигурации
  3. [*provider.tf*](https://gitlab.com/netology-devops-project/terraform/-/blob/main/provider.tf?ref_type=heads) - описывает провайдера, версию и используемый бэкенд
  4. [*variables.tf*](https://gitlab.com/netology-devops-project/terraform/-/blob/main/variables.tf?ref_type=heads) - содержит переменные

Было принято решение не использовать в основном манифесте "захардкоженные" значения. Все включая *IP-адресацию*, *публичный SSH-ключ*, *название образа*, *ресурсы* и *количество инстансов* вынесено в  *variables.tf*.

Для *мастер-нод* была выбрана конфигурация с *4 CPU*, *8GB RAM*, *Core Fraction 20%*, *Disk Size 64GB*.

Для *воркер-нод* была выбрана конфигурация с *4 CPU*, *4GB RAM*, *Core Fraction 20%*, *Disk Size 64GB*, также эти машины создаются прерываемыми.

Манифесты создают *VPC* c названием *"kubenet"* и подсетями в двух зонах доступности:
  * *subnet-a* - *ru-central1-a*, *10.10.10.0/24*.
  * *subnet-b* - *ru-central1-b*, *10.10.11.0/24*.

Для рандомизации распределения виртуальных машин по подсетям был выбран плагин *random_integer* который отдает случайный индекс подсети и зоны для каждого *Compute-ресурса*.

Перед тем как инициализировать *Terraform*, необходимо экспортировать переменные:
  ```bash
  [manokhin@local:.../terraform(main)]> export YC_TOKEN=$(yc iam create-token)
  [manokhin@local:.../terraform(main)]> export YC_CLOUD_ID=$(yc config get cloud-id)
  [manokhin@local:.../terraform(main)]> export YC_FOLDER_ID=$(yc config get folder-id)
  [manokhin@local:.../terraform(main)]> export TF_VAR_ssh_pub_key=$(cat ~/.ssh/id_rsa.pub)
  ```

<details>
  <summary>Лог terraform init && terraform apply -auto-approve (тестовый запуск с выключенным бэкендом)</summary>

  ```bash
  [manokhin@local:.../terraform(main)]> terraform init && terraform apply -auto-approve
  
  Initializing the backend...
  
  Initializing provider plugins...
  - Finding yandex-cloud/yandex versions matching "0.117.0"...
  - Finding latest version of hashicorp/random...
  - Installing yandex-cloud/yandex v0.117.0...
  - Installed yandex-cloud/yandex v0.117.0 (unauthenticated)
  - Installing hashicorp/random v3.6.2...
  - Installed hashicorp/random v3.6.2 (unauthenticated)
  
  Terraform has created a lock file .terraform.lock.hcl to record the provider
  selections it made above. Include this file in your version control repository
  so that Terraform can guarantee to make the same selections by default when
  you run "terraform init" in the future.
  
  ╷
  │ Warning: Incomplete lock file information for providers
  │ 
  │ Due to your customized provider installation methods, Terraform was forced to calculate lock file checksums locally for the following providers:
  │   - hashicorp/random
  │   - yandex-cloud/yandex
  │ 
  │ The current .terraform.lock.hcl file only includes checksums for linux_amd64, so Terraform running on another platform will fail to install these providers.
  │ 
  │ To calculate additional checksums for another platform, run:
  │   terraform providers lock -platform=linux_amd64
  │ (where linux_amd64 is the platform to generate)
  ╵
  
  Terraform has been successfully initialized!
  
  You may now begin working with Terraform. Try running "terraform plan" to see
  any changes that are required for your infrastructure. All Terraform commands
  should now work.
  
  If you ever set or change modules or backend configuration for Terraform,
  rerun this command to reinitialize your working directory. If you forget, other
  commands will detect it and remind you to do so if necessary.
  data.yandex_compute_image.image: Reading...
  data.yandex_compute_image.image: Read complete after 4s [id=fd8e5oous6ulsjrcclqf]
  
  Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
    + create
  
  Terraform will perform the following actions:
  
    # random_integer.random_master_subnet_index[0] will be created
    + resource "random_integer" "random_master_subnet_index" {
        + id     = (known after apply)
        + max    = 1
        + min    = 0
        + result = (known after apply)
      }
  
    # random_integer.random_master_subnet_index[1] will be created
    + resource "random_integer" "random_master_subnet_index" {
        + id     = (known after apply)
        + max    = 1
        + min    = 0
        + result = (known after apply)
      }
  
    # random_integer.random_master_subnet_index[2] will be created
    + resource "random_integer" "random_master_subnet_index" {
        + id     = (known after apply)
        + max    = 1
        + min    = 0
        + result = (known after apply)
      }
  
    # random_integer.random_worker_subnet_index[0] will be created
    + resource "random_integer" "random_worker_subnet_index" {
        + id     = (known after apply)
        + max    = 1
        + min    = 0
        + result = (known after apply)
      }
  
    # random_integer.random_worker_subnet_index[1] will be created
    + resource "random_integer" "random_worker_subnet_index" {
        + id     = (known after apply)
        + max    = 1
        + min    = 0
        + result = (known after apply)
      }
  
    # yandex_compute_instance.k8s_master[0] will be created
    + resource "yandex_compute_instance" "k8s_master" {
        + allow_stopping_for_update = false
        + created_at                = (known after apply)
        + folder_id                 = (known after apply)
        + fqdn                      = (known after apply)
        + gpu_cluster_id            = (known after apply)
        + hostname                  = "k8s-master-1"
        + id                        = (known after apply)
        + maintenance_grace_period  = (known after apply)
        + maintenance_policy        = (known after apply)
        + metadata                  = {
            + "ssh-keys" = "ubuntu:ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDwEKoBJ0rHhXmzfBHj7Nm36uA1CzVKLCUvjuYaW7vDNcEd80XLD8QcVlx84kqkXWA1M5zpGDzDafjkkO9gXeNmh/Fgbz4CQY6DtNazu/T2lnYT7mKumcE2B8qkmUFVq+XF/9zlR2DqBlYpwFAga+pr2+ssI9pR6FjmWpitgepiWZ93Vz3H4o5ZEitIiyNLPMYqy/3CprwLeMrjkL5iIcWeqoGGyqieT/dym8UaChFvBzogSi3zh9DlVyqoQmdROXEf+w6LakyYDopB/iAcAZ5prF28nk9iVLvRyvf/DTEJpOA66Ev7GIbwtWFojf1YvY3xVs6Ng1I974Bq9AOn0M901DCbK8d/Q/v7Dyb/fRzSNVym9iX29rgGN8QIbSzUyh3buCdSzcpphXXHW2qG7xU5Dw7xkUyCPpjIIhF3xqwiD0bhfh5PuHsf4piS47ZklLSzQQcr2XTUjFZhAARPH5EKs5dB7px0abEiONje8VjvcviBNW1qmXgsuVKQWFwKazuMmnAWQC1cAr9ZePvmI19PyO+0umqJ2VPYffu9dCU5lrRc2dxnFh4mhbxZlwSJDHBysC7ep+i1rjX++NfhoF5jaTdfKia9ywqok7CJnHiMRGGGsiXvHGkpY1kc7MmxyiNdIeYp/ucx+8EUVCa6JU4v/hVyt8wF5pA0izrs+YhxVw== ivan@manokhin\r"
          }
        + name                      = "k8s-master-1"
        + network_acceleration_type = "standard"
        + platform_id               = "standard-v1"
        + service_account_id        = (known after apply)
        + status                    = (known after apply)
        + zone                      = (known after apply)
  
        + boot_disk {
            + auto_delete = true
            + device_name = (known after apply)
            + disk_id     = (known after apply)
            + mode        = (known after apply)
  
            + initialize_params {
                + block_size  = (known after apply)
                + description = (known after apply)
                + image_id    = "fd8e5oous6ulsjrcclqf"
                + name        = (known after apply)
                + size        = 64
                + snapshot_id = (known after apply)
                + type        = "network-hdd"
              }
          }
  
        + network_interface {
            + index              = (known after apply)
            + ip_address         = (known after apply)
            + ipv4               = true
            + ipv6               = (known after apply)
            + ipv6_address       = (known after apply)
            + mac_address        = (known after apply)
            + nat                = true
            + nat_ip_address     = (known after apply)
            + nat_ip_version     = (known after apply)
            + security_group_ids = (known after apply)
            + subnet_id          = (known after apply)
          }
  
        + resources {
            + core_fraction = 20
            + cores         = 4
            + memory        = 8
          }
      }
  
    # yandex_compute_instance.k8s_master[1] will be created
    + resource "yandex_compute_instance" "k8s_master" {
        + allow_stopping_for_update = false
        + created_at                = (known after apply)
        + folder_id                 = (known after apply)
        + fqdn                      = (known after apply)
        + gpu_cluster_id            = (known after apply)
        + hostname                  = "k8s-master-2"
        + id                        = (known after apply)
        + maintenance_grace_period  = (known after apply)
        + maintenance_policy        = (known after apply)
        + metadata                  = {
            + "ssh-keys" = "ubuntu:ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDwEKoBJ0rHhXmzfBHj7Nm36uA1CzVKLCUvjuYaW7vDNcEd80XLD8QcVlx84kqkXWA1M5zpGDzDafjkkO9gXeNmh/Fgbz4CQY6DtNazu/T2lnYT7mKumcE2B8qkmUFVq+XF/9zlR2DqBlYpwFAga+pr2+ssI9pR6FjmWpitgepiWZ93Vz3H4o5ZEitIiyNLPMYqy/3CprwLeMrjkL5iIcWeqoGGyqieT/dym8UaChFvBzogSi3zh9DlVyqoQmdROXEf+w6LakyYDopB/iAcAZ5prF28nk9iVLvRyvf/DTEJpOA66Ev7GIbwtWFojf1YvY3xVs6Ng1I974Bq9AOn0M901DCbK8d/Q/v7Dyb/fRzSNVym9iX29rgGN8QIbSzUyh3buCdSzcpphXXHW2qG7xU5Dw7xkUyCPpjIIhF3xqwiD0bhfh5PuHsf4piS47ZklLSzQQcr2XTUjFZhAARPH5EKs5dB7px0abEiONje8VjvcviBNW1qmXgsuVKQWFwKazuMmnAWQC1cAr9ZePvmI19PyO+0umqJ2VPYffu9dCU5lrRc2dxnFh4mhbxZlwSJDHBysC7ep+i1rjX++NfhoF5jaTdfKia9ywqok7CJnHiMRGGGsiXvHGkpY1kc7MmxyiNdIeYp/ucx+8EUVCa6JU4v/hVyt8wF5pA0izrs+YhxVw== ivan@manokhin\r"
          }
        + name                      = "k8s-master-2"
        + network_acceleration_type = "standard"
        + platform_id               = "standard-v1"
        + service_account_id        = (known after apply)
        + status                    = (known after apply)
        + zone                      = (known after apply)
  
        + boot_disk {
            + auto_delete = true
            + device_name = (known after apply)
            + disk_id     = (known after apply)
            + mode        = (known after apply)
  
            + initialize_params {
                + block_size  = (known after apply)
                + description = (known after apply)
                + image_id    = "fd8e5oous6ulsjrcclqf"
                + name        = (known after apply)
                + size        = 64
                + snapshot_id = (known after apply)
                + type        = "network-hdd"
              }
          }
  
        + network_interface {
            + index              = (known after apply)
            + ip_address         = (known after apply)
            + ipv4               = true
            + ipv6               = (known after apply)
            + ipv6_address       = (known after apply)
            + mac_address        = (known after apply)
            + nat                = true
            + nat_ip_address     = (known after apply)
            + nat_ip_version     = (known after apply)
            + security_group_ids = (known after apply)
            + subnet_id          = (known after apply)
          }
  
        + resources {
            + core_fraction = 20
            + cores         = 4
            + memory        = 8
          }
      }
  
    # yandex_compute_instance.k8s_master[2] will be created
    + resource "yandex_compute_instance" "k8s_master" {
        + allow_stopping_for_update = false
        + created_at                = (known after apply)
        + folder_id                 = (known after apply)
        + fqdn                      = (known after apply)
        + gpu_cluster_id            = (known after apply)
        + hostname                  = "k8s-master-3"
        + id                        = (known after apply)
        + maintenance_grace_period  = (known after apply)
        + maintenance_policy        = (known after apply)
        + metadata                  = {
            + "ssh-keys" = "ubuntu:ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDwEKoBJ0rHhXmzfBHj7Nm36uA1CzVKLCUvjuYaW7vDNcEd80XLD8QcVlx84kqkXWA1M5zpGDzDafjkkO9gXeNmh/Fgbz4CQY6DtNazu/T2lnYT7mKumcE2B8qkmUFVq+XF/9zlR2DqBlYpwFAga+pr2+ssI9pR6FjmWpitgepiWZ93Vz3H4o5ZEitIiyNLPMYqy/3CprwLeMrjkL5iIcWeqoGGyqieT/dym8UaChFvBzogSi3zh9DlVyqoQmdROXEf+w6LakyYDopB/iAcAZ5prF28nk9iVLvRyvf/DTEJpOA66Ev7GIbwtWFojf1YvY3xVs6Ng1I974Bq9AOn0M901DCbK8d/Q/v7Dyb/fRzSNVym9iX29rgGN8QIbSzUyh3buCdSzcpphXXHW2qG7xU5Dw7xkUyCPpjIIhF3xqwiD0bhfh5PuHsf4piS47ZklLSzQQcr2XTUjFZhAARPH5EKs5dB7px0abEiONje8VjvcviBNW1qmXgsuVKQWFwKazuMmnAWQC1cAr9ZePvmI19PyO+0umqJ2VPYffu9dCU5lrRc2dxnFh4mhbxZlwSJDHBysC7ep+i1rjX++NfhoF5jaTdfKia9ywqok7CJnHiMRGGGsiXvHGkpY1kc7MmxyiNdIeYp/ucx+8EUVCa6JU4v/hVyt8wF5pA0izrs+YhxVw== ivan@manokhin\r"
          }
        + name                      = "k8s-master-3"
        + network_acceleration_type = "standard"
        + platform_id               = "standard-v1"
        + service_account_id        = (known after apply)
        + status                    = (known after apply)
        + zone                      = (known after apply)
  
        + boot_disk {
            + auto_delete = true
            + device_name = (known after apply)
            + disk_id     = (known after apply)
            + mode        = (known after apply)
  
            + initialize_params {
                + block_size  = (known after apply)
                + description = (known after apply)
                + image_id    = "fd8e5oous6ulsjrcclqf"
                + name        = (known after apply)
                + size        = 64
                + snapshot_id = (known after apply)
                + type        = "network-hdd"
              }
          }
  
        + network_interface {
            + index              = (known after apply)
            + ip_address         = (known after apply)
            + ipv4               = true
            + ipv6               = (known after apply)
            + ipv6_address       = (known after apply)
            + mac_address        = (known after apply)
            + nat                = true
            + nat_ip_address     = (known after apply)
            + nat_ip_version     = (known after apply)
            + security_group_ids = (known after apply)
            + subnet_id          = (known after apply)
          }
  
        + resources {
            + core_fraction = 20
            + cores         = 4
            + memory        = 8
          }
      }
  
    # yandex_compute_instance.k8s_worker[0] will be created
    + resource "yandex_compute_instance" "k8s_worker" {
        + allow_stopping_for_update = true
        + created_at                = (known after apply)
        + folder_id                 = (known after apply)
        + fqdn                      = (known after apply)
        + gpu_cluster_id            = (known after apply)
        + hostname                  = "k8s-worker-1"
        + id                        = (known after apply)
        + maintenance_grace_period  = (known after apply)
        + maintenance_policy        = (known after apply)
        + metadata                  = {
            + "ssh-keys" = "ubuntu:ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDwEKoBJ0rHhXmzfBHj7Nm36uA1CzVKLCUvjuYaW7vDNcEd80XLD8QcVlx84kqkXWA1M5zpGDzDafjkkO9gXeNmh/Fgbz4CQY6DtNazu/T2lnYT7mKumcE2B8qkmUFVq+XF/9zlR2DqBlYpwFAga+pr2+ssI9pR6FjmWpitgepiWZ93Vz3H4o5ZEitIiyNLPMYqy/3CprwLeMrjkL5iIcWeqoGGyqieT/dym8UaChFvBzogSi3zh9DlVyqoQmdROXEf+w6LakyYDopB/iAcAZ5prF28nk9iVLvRyvf/DTEJpOA66Ev7GIbwtWFojf1YvY3xVs6Ng1I974Bq9AOn0M901DCbK8d/Q/v7Dyb/fRzSNVym9iX29rgGN8QIbSzUyh3buCdSzcpphXXHW2qG7xU5Dw7xkUyCPpjIIhF3xqwiD0bhfh5PuHsf4piS47ZklLSzQQcr2XTUjFZhAARPH5EKs5dB7px0abEiONje8VjvcviBNW1qmXgsuVKQWFwKazuMmnAWQC1cAr9ZePvmI19PyO+0umqJ2VPYffu9dCU5lrRc2dxnFh4mhbxZlwSJDHBysC7ep+i1rjX++NfhoF5jaTdfKia9ywqok7CJnHiMRGGGsiXvHGkpY1kc7MmxyiNdIeYp/ucx+8EUVCa6JU4v/hVyt8wF5pA0izrs+YhxVw== ivan@manokhin\r"
          }
        + name                      = "k8s-worker-1"
        + network_acceleration_type = "standard"
        + platform_id               = "standard-v1"
        + service_account_id        = (known after apply)
        + status                    = (known after apply)
        + zone                      = (known after apply)
  
        + boot_disk {
            + auto_delete = true
            + device_name = (known after apply)
            + disk_id     = (known after apply)
            + mode        = (known after apply)
  
            + initialize_params {
                + block_size  = (known after apply)
                + description = (known after apply)
                + image_id    = "fd8e5oous6ulsjrcclqf"
                + name        = (known after apply)
                + size        = 64
                + snapshot_id = (known after apply)
                + type        = "network-hdd"
              }
          }
  
        + network_interface {
            + index              = (known after apply)
            + ip_address         = (known after apply)
            + ipv4               = true
            + ipv6               = (known after apply)
            + ipv6_address       = (known after apply)
            + mac_address        = (known after apply)
            + nat                = true
            + nat_ip_address     = (known after apply)
            + nat_ip_version     = (known after apply)
            + security_group_ids = (known after apply)
            + subnet_id          = (known after apply)
          }
  
        + resources {
            + core_fraction = 20
            + cores         = 4
            + memory        = 4
          }
  
        + scheduling_policy {
            + preemptible = true
          }
      }
  
    # yandex_compute_instance.k8s_worker[1] will be created
    + resource "yandex_compute_instance" "k8s_worker" {
        + allow_stopping_for_update = true
        + created_at                = (known after apply)
        + folder_id                 = (known after apply)
        + fqdn                      = (known after apply)
        + gpu_cluster_id            = (known after apply)
        + hostname                  = "k8s-worker-2"
        + id                        = (known after apply)
        + maintenance_grace_period  = (known after apply)
        + maintenance_policy        = (known after apply)
        + metadata                  = {
            + "ssh-keys" = "ubuntu:ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDwEKoBJ0rHhXmzfBHj7Nm36uA1CzVKLCUvjuYaW7vDNcEd80XLD8QcVlx84kqkXWA1M5zpGDzDafjkkO9gXeNmh/Fgbz4CQY6DtNazu/T2lnYT7mKumcE2B8qkmUFVq+XF/9zlR2DqBlYpwFAga+pr2+ssI9pR6FjmWpitgepiWZ93Vz3H4o5ZEitIiyNLPMYqy/3CprwLeMrjkL5iIcWeqoGGyqieT/dym8UaChFvBzogSi3zh9DlVyqoQmdROXEf+w6LakyYDopB/iAcAZ5prF28nk9iVLvRyvf/DTEJpOA66Ev7GIbwtWFojf1YvY3xVs6Ng1I974Bq9AOn0M901DCbK8d/Q/v7Dyb/fRzSNVym9iX29rgGN8QIbSzUyh3buCdSzcpphXXHW2qG7xU5Dw7xkUyCPpjIIhF3xqwiD0bhfh5PuHsf4piS47ZklLSzQQcr2XTUjFZhAARPH5EKs5dB7px0abEiONje8VjvcviBNW1qmXgsuVKQWFwKazuMmnAWQC1cAr9ZePvmI19PyO+0umqJ2VPYffu9dCU5lrRc2dxnFh4mhbxZlwSJDHBysC7ep+i1rjX++NfhoF5jaTdfKia9ywqok7CJnHiMRGGGsiXvHGkpY1kc7MmxyiNdIeYp/ucx+8EUVCa6JU4v/hVyt8wF5pA0izrs+YhxVw== ivan@manokhin\r"
          }
        + name                      = "k8s-worker-2"
        + network_acceleration_type = "standard"
        + platform_id               = "standard-v1"
        + service_account_id        = (known after apply)
        + status                    = (known after apply)
        + zone                      = (known after apply)
  
        + boot_disk {
            + auto_delete = true
            + device_name = (known after apply)
            + disk_id     = (known after apply)
            + mode        = (known after apply)
  
            + initialize_params {
                + block_size  = (known after apply)
                + description = (known after apply)
                + image_id    = "fd8e5oous6ulsjrcclqf"
                + name        = (known after apply)
                + size        = 64
                + snapshot_id = (known after apply)
                + type        = "network-hdd"
              }
          }
  
        + network_interface {
            + index              = (known after apply)
            + ip_address         = (known after apply)
            + ipv4               = true
            + ipv6               = (known after apply)
            + ipv6_address       = (known after apply)
            + mac_address        = (known after apply)
            + nat                = true
            + nat_ip_address     = (known after apply)
            + nat_ip_version     = (known after apply)
            + security_group_ids = (known after apply)
            + subnet_id          = (known after apply)
          }
  
        + resources {
            + core_fraction = 20
            + cores         = 4
            + memory        = 4
          }
  
        + scheduling_policy {
            + preemptible = true
          }
      }
  
    # yandex_vpc_network.network will be created
    + resource "yandex_vpc_network" "network" {
        + created_at                = (known after apply)
        + default_security_group_id = (known after apply)
        + folder_id                 = (known after apply)
        + id                        = (known after apply)
        + labels                    = (known after apply)
        + name                      = "kubenet"
        + subnet_ids                = (known after apply)
      }
  
    # yandex_vpc_subnet.subnets["subnet-a"] will be created
    + resource "yandex_vpc_subnet" "subnets" {
        + created_at     = (known after apply)
        + folder_id      = (known after apply)
        + id             = (known after apply)
        + labels         = (known after apply)
        + name           = "subnet-a"
        + network_id     = (known after apply)
        + v4_cidr_blocks = [
            + "10.10.10.0/24",
          ]
        + v6_cidr_blocks = (known after apply)
        + zone           = "ru-central1-a"
      }
  
    # yandex_vpc_subnet.subnets["subnet-b"] will be created
    + resource "yandex_vpc_subnet" "subnets" {
        + created_at     = (known after apply)
        + folder_id      = (known after apply)
        + id             = (known after apply)
        + labels         = (known after apply)
        + name           = "subnet-b"
        + network_id     = (known after apply)
        + v4_cidr_blocks = [
            + "10.10.11.0/24",
          ]
        + v6_cidr_blocks = (known after apply)
        + zone           = "ru-central1-b"
      }
  
  Plan: 13 to add, 0 to change, 0 to destroy.
  
  Changes to Outputs:
    + instance_ips = [
        + (known after apply),
        + (known after apply),
        + (known after apply),
        + (known after apply),
        + (known after apply),
      ]
  yandex_vpc_network.network: Creating...
  yandex_vpc_network.network: Creation complete after 7s [id=enp0qlm7jgpl4f93jfo5]
  yandex_vpc_subnet.subnets["subnet-a"]: Creating...
  yandex_vpc_subnet.subnets["subnet-b"]: Creating...
  yandex_vpc_subnet.subnets["subnet-b"]: Creation complete after 1s [id=e2liies31ob1qbtujhfa]
  yandex_vpc_subnet.subnets["subnet-a"]: Creation complete after 1s [id=e9b1tngg1disqmude9hr]
  random_integer.random_worker_subnet_index[0]: Creating...
  random_integer.random_worker_subnet_index[1]: Creating...
  random_integer.random_master_subnet_index[1]: Creating...
  random_integer.random_master_subnet_index[0]: Creating...
  random_integer.random_master_subnet_index[2]: Creating...
  random_integer.random_master_subnet_index[0]: Creation complete after 0s [id=0]
  random_integer.random_worker_subnet_index[1]: Creation complete after 0s [id=0]
  random_integer.random_master_subnet_index[1]: Creation complete after 0s [id=0]
  random_integer.random_master_subnet_index[2]: Creation complete after 0s [id=1]
  random_integer.random_worker_subnet_index[0]: Creation complete after 0s [id=1]
  yandex_compute_instance.k8s_worker[0]: Creating...
  yandex_compute_instance.k8s_worker[1]: Creating...
  yandex_compute_instance.k8s_master[1]: Creating...
  yandex_compute_instance.k8s_master[0]: Creating...
  yandex_compute_instance.k8s_master[2]: Creating...
  yandex_compute_instance.k8s_worker[0]: Still creating... [10s elapsed]
  yandex_compute_instance.k8s_master[1]: Still creating... [10s elapsed]
  yandex_compute_instance.k8s_worker[1]: Still creating... [10s elapsed]
  yandex_compute_instance.k8s_master[2]: Still creating... [10s elapsed]
  yandex_compute_instance.k8s_master[0]: Still creating... [10s elapsed]
  yandex_compute_instance.k8s_worker[0]: Still creating... [20s elapsed]
  yandex_compute_instance.k8s_worker[1]: Still creating... [20s elapsed]
  yandex_compute_instance.k8s_master[1]: Still creating... [20s elapsed]
  yandex_compute_instance.k8s_master[2]: Still creating... [20s elapsed]
  yandex_compute_instance.k8s_master[0]: Still creating... [20s elapsed]
  yandex_compute_instance.k8s_worker[0]: Still creating... [30s elapsed]
  yandex_compute_instance.k8s_master[1]: Still creating... [30s elapsed]
  yandex_compute_instance.k8s_worker[1]: Still creating... [30s elapsed]
  yandex_compute_instance.k8s_master[0]: Still creating... [30s elapsed]
  yandex_compute_instance.k8s_master[2]: Still creating... [30s elapsed]
  yandex_compute_instance.k8s_worker[1]: Creation complete after 36s [id=fhm7uu2un6ejft4cqrei]
  yandex_compute_instance.k8s_master[1]: Creation complete after 36s [id=fhm8c8pfunsr9gj8epa0]
  yandex_compute_instance.k8s_master[0]: Creation complete after 37s [id=fhmbok2gb3093jp6p48k]
  yandex_compute_instance.k8s_worker[0]: Still creating... [40s elapsed]
  yandex_compute_instance.k8s_master[2]: Still creating... [40s elapsed]
  yandex_compute_instance.k8s_worker[0]: Still creating... [50s elapsed]
  yandex_compute_instance.k8s_master[2]: Still creating... [50s elapsed]
  yandex_compute_instance.k8s_master[2]: Creation complete after 56s [id=epdvcha7462lcm7jv0js]
  yandex_compute_instance.k8s_worker[0]: Still creating... [1m0s elapsed]
  yandex_compute_instance.k8s_worker[0]: Creation complete after 1m2s [id=epdr2b0v382rliiqh8ar]
  
  Apply complete! Resources: 13 added, 0 changed, 0 destroyed.
  
  Outputs:
  
  instance_ips = [
    "k8s-master-1 : 10.10.10.3 : 84.201.172.67",
    "k8s-master-2 : 10.10.10.8 : 158.160.113.180",
    "k8s-master-3 : 10.10.11.10 : 158.160.21.27",
    "k8s-worker-1 : 10.10.11.17 : 84.252.143.156",
    "k8s-worker-2 : 10.10.10.29 : 158.160.58.8",
  ]
  ```
</details>

Перед тем как запустить применение конфигурации через *Terraform Cloud*, необходимо создать проект и воркспейс и прописать переменные:
![](./assets/images/terraform_cloud_vars.png)

Ручной запуск *Plan and Apply* через интерфейс *Terraform Cloud*:
![](./assets/images/terraform_cloud_plan.png)

![](./assets/images/terraform_cloud_apply.png)

Созданные ресурсы в *Yandex.Cloud*:
![](./assets/images/yandex_cloud_compute.png)

![](./assets/images/yandex_cloud_vpc.png)

Тест доступности виртуальной машины по *SSH*:
  ```bash
  [manokhin@local:.../terraform(main)]> ssh ubuntu@158.160.91.85
  The authenticity of host '158.160.91.85 (158.160.91.85)' can't be established.
  ED25519 key fingerprint is SHA256:zF6ycIxoJ2AEFoqvy92lrIwiajI2p5Aq920F8na3o2w.
  This key is not known by any other names
  Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
  Warning: Permanently added '158.160.91.85' (ED25519) to the list of known hosts.
  Enter passphrase for key '/home/manokhin/.ssh/id_rsa': 
  Welcome to Ubuntu 24.04 LTS (GNU/Linux 6.8.0-35-generic x86_64)
  
   * Documentation:  https://help.ubuntu.com
   * Management:     https://landscape.canonical.com
   * Support:        https://ubuntu.com/pro
  
   System information as of Wed Jun 19 04:52:22 AM UTC 2024
  
    System load:  0.08              Processes:             151
    Usage of /:   4.4% of 62.90GB   Users logged in:       0
    Memory usage: 2%                IPv4 address for eth0: 10.10.11.32
    Swap usage:   0%
  
  ```

Доступность хостов между разными подсетями:
  ```bash
  ubuntu@k8s-master-3:~$ ping k8s-worker-2 -c 2
  PING k8s-worker-2.ru-central1.internal (10.10.11.12) 56(84) bytes of data.
  64 bytes from k8s-worker-2.ru-central1.internal (10.10.11.12): icmp_seq=1 ttl=61 time=0.573 ms
  64 bytes from k8s-worker-2.ru-central1.internal (10.10.11.12): icmp_seq=2 ttl=61 time=0.564 ms
  
  --- k8s-worker-2.ru-central1.internal ping statistics ---
  2 packets transmitted, 2 received, 0% packet loss, time 1061ms
  rtt min/avg/max/mdev = 0.564/0.568/0.573/0.004 ms
  ubuntu@k8s-master-3:~$ ping k8s-master-1 -c 2
  PING k8s-master-1.ru-central1.internal (10.10.11.13) 56(84) bytes of data.
  64 bytes from k8s-master-1.ru-central1.internal (10.10.11.13): icmp_seq=1 ttl=61 time=1.94 ms
  64 bytes from k8s-master-1.ru-central1.internal (10.10.11.13): icmp_seq=2 ttl=61 time=0.603 ms
  
  --- k8s-master-1.ru-central1.internal ping statistics ---
  2 packets transmitted, 2 received, 0% packet loss, time 1001ms
  rtt min/avg/max/mdev = 0.603/1.272/1.942/0.669 ms
  ```

*Terraform Plan* по коммиту (также включен и *Apply*, но изменений не было, т.к. обновлен был только *.gitignore*):
  * В интерфейсе *GitLab*:  
    ![](./assets/images/gitlab_terraform_plan_on_commit.png)
  * В интерфейсе *Terraform Cloud*:  
    ![](./assets/images/terraform_cloud_plan_on_commit.png)

**Инфраструктура подготовлена, можно приступать к следующему этапу.**

## <a name="second-stage"></a>  4. Создание Kubernetes кластера

### Задание

На этом этапе необходимо создать [*Kubernetes*](https://kubernetes.io/ru/docs/concepts/overview/what-is-kubernetes/) кластер на базе предварительно созданной инфраструктуры. Требуется обеспечить доступ к ресурсам из Интернета.

Это можно сделать двумя способами:
1. Рекомендуемый вариант: самостоятельная установка *Kubernetes* кластера.  
   а. При помощи *Terraform* подготовить как минимум 3 виртуальных машины *Compute Cloud* для создания *Kubernetes-кластера*. Тип виртуальной машины следует выбрать самостоятельно с учётом требовании к производительности и стоимости. Если в дальнейшем поймете, что необходимо сменить тип инстанса, используйте *Terraform* для внесения изменений.  
   б. Подготовить [*Ansible*](https://www.ansible.com/) конфигурации, можно воспользоваться, например [*Kubespray*](https://kubernetes.io/docs/setup/production-environment/tools/kubespray/).  
   в. Задеплоить *Kubernetes* на подготовленные ранее инстансы, в случае нехватки каких-либо ресурсов вы всегда можете создать их при помощи *Terraform*.  
2. Альтернативный вариант: воспользуйтесь сервисом [*Yandex Managed Service for Kubernetes*](https://cloud.yandex.ru/services/managed-kubernetes)  
   а. С помощью *terraform resource* для [*kubernetes*](https://registry.terraform.io/providers/yandex-cloud/yandex/latest/docs/resources/kubernetes_cluster) создать **региональный** мастер *s* с размещением нод в разных 3 подсетях.  
   б. С помощью *terraform resource* для [*kubernetes node group*](https://registry.terraform.io/providers/yandex-cloud/yandex/latest/docs/resources/kubernetes_node_group).  

Ожидаемый результат:
1. Работоспособный *Kubernetes* кластер.
2. В файле `~/.kube/config` находятся данные для доступа к кластеру.
3. Команда `kubectl get pods --all-namespaces` отрабатывает без ошибок.

---

### Результат

Для решения данной задачи был выбран способ развертки через *Kubespray*. Такое решения было принято в связи с тем, что он предоставляет удобные возможности и простоту конфигурации. Но даже с параметрами по умолчанию, он обеспечивает *K8s-кластер* продакшн уровня, а развертка кластера по официальной инструкции осуществлялась в рамках прохождения курса.

[Клонированный репозиторий kubespray (добавлен *inventory*)](https://gitlab.com/netology-devops-project/kubespray)

Перед тем как приступить к развертке, для более удобного получения доступа к кластеру (и, в какой-то мере, целях абстракции от IP-адресации, так как она не постоянная и может измениться), были внесены записи в DNS.

Настройки DNS:
![](./assets/images/dns_config.png)

3 записи для хоста *k8s* созданы для того что бы использовать в конфигурации *kubectl* и доступ был к любой из работающих мастер-нод, без необходимости изменения конфигурации.

Домен верхнего уровня: manokh.in

После клонирования репозитория необходимо создать виртуальное окружение для *Ansible* (данный способ является рекомендуемым):
```bash
[manokhin@local:.../netology-devops-project()]> VENVDIR=kubespray-venv
[manokhin@local:.../netology-devops-project()]> KUBESPRAYDIR=kubespray
[manokhin@local:.../netology-devops-project()]> python3 -m venv $VENVDIR
[manokhin@local:.../netology-devops-project()]> source $VENVDIR/bin/activate
(kubespray-venv) [manokhin@local:.../kubespray(master)]> cd $KUBESPRAYDIR
(kubespray-venv) [manokhin@local:.../kubespray(master)]> pip install -U -r requirements.txt
(kubespray-venv) [manokhin@local:.../kubespray(master)]> pip3 install ruamel.yaml # необходим для генерации inventory
```

Далее сгенерируем *inventory*:
```bash
(kubespray-venv) [manokhin@local:.../kubespray(master)]> cp -rfp inventory/sample inventory/mycluster
(kubespray-venv) [manokhin@local:.../kubespray(master)]> declare -a IPS=(k8s-master-1,10.10.11.13,master-1.k8s.manokh.in k8s-master-2,10.10.10.12,master-2.k8s.manokh.in k8s-master-3,10.10.11.32,master-3.k8s.manokh.in k8s-worker-1,10.10.11.16,worker-1.k8s.manokh.in k8s-worker-2,10.10.11.12,worker-2.k8s.manokh.in)
(kubespray-venv) [manokhin@local:.../kubespray(master)]> CONFIG_FILE=inventory/k8s/hosts.yaml python3 contrib/inventory_builder/inventory.py ${IPS[@]}
DEBUG: Adding group all
DEBUG: Adding group kube_control_plane
DEBUG: Adding group kube_node
DEBUG: Adding group etcd
DEBUG: Adding group k8s_cluster
DEBUG: Adding group calico_rr
DEBUG: adding host k8s-master-1 to group all
DEBUG: adding host k8s-master-2 to group all
DEBUG: adding host k8s-master-3 to group all
DEBUG: adding host k8s-worker-1 to group all
DEBUG: adding host k8s-worker-2 to group all
DEBUG: adding host k8s-master-1 to group etcd
DEBUG: adding host k8s-master-2 to group etcd
DEBUG: adding host k8s-master-3 to group etcd
DEBUG: adding host k8s-master-1 to group kube_control_plane
DEBUG: adding host k8s-master-2 to group kube_control_plane
DEBUG: adding host k8s-master-1 to group kube_node
DEBUG: adding host k8s-master-2 to group kube_node
DEBUG: adding host k8s-master-3 to group kube_node
DEBUG: adding host k8s-worker-1 to group kube_node
DEBUG: adding host k8s-worker-2 to group kube_node
```

Сгенерированный *inventory* необходимо отредактировать, изменить *access_ip* на приватные адреса, и добавить блок *supplementary_addresses_in_ssl_keys* с хостами для которых требуется генерировать сертификаты.

[Готовый *hosts.yml*](https://gitlab.com/netology-devops-project/kubespray/-/blob/master/inventory/k8s/hosts.yaml?ref_type=heads)

[Включенная настройка *Nginx Ingress*](https://gitlab.com/netology-devops-project/kubespray/-/blob/master/inventory/k8s/group_vars/k8s_cluster/addons.yml?ref_type=heads#L100)

После этих изменений *Kubespray* готов к запуску.

[Полный лог создания кластера](https://pastebin.com/LN34RHrp)

По завершении настройки с мастер-ноды была скопирована конфигурация для *kubectl*, в ней изменен адрес *API-сервера* на *https://k8s.manokh.in:6443*.

Проверка кластера:
* Статус кластера:
  ```bash
  (kubespray-venv) [manokhin@local:.../kubespray(master)]> kubectl cluster-info
  Kubernetes control plane is running at https://k8s.manokh.in:6443
  
  To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.
  ```
* Статус нод кластера:
  ```bash
  (kubespray-venv) [manokhin@local:.../kubespray(master)]> kubectl get nodes -o wide
  NAME           STATUS   ROLES           AGE     VERSION   INTERNAL-IP   EXTERNAL-IP   OS-IMAGE           KERNEL-VERSION     CONTAINER-RUNTIME
  k8s-master-1   Ready    control-plane   9m46s   v1.29.5   10.10.11.13   <none>        Ubuntu 24.04 LTS   6.8.0-35-generic   containerd://1.7.16
  k8s-master-2   Ready    control-plane   9m      v1.29.5   10.10.10.12   <none>        Ubuntu 24.04 LTS   6.8.0-35-generic   containerd://1.7.16
  k8s-master-3   Ready    control-plane   8m36s   v1.29.5   10.10.11.32   <none>        Ubuntu 24.04 LTS   6.8.0-35-generic   containerd://1.7.16
  k8s-worker-1   Ready    <none>          7m41s   v1.29.5   10.10.11.16   <none>        Ubuntu 24.04 LTS   6.8.0-35-generic   containerd://1.7.16
  k8s-worker-2   Ready    <none>          7m40s   v1.29.5   10.10.11.12   <none>        Ubuntu 24.04 LTS   6.8.0-35-generic   containerd://1.7.16
  ```
* Статус всех подов кластера:
  ```bash
  (kubespray-venv) [manokhin@local:.../kubespray(master)]> kubectl get pods -o wide --all-namespaces
  NAMESPACE       NAME                                       READY   STATUS    RESTARTS        AGE     IP               NODE           NOMINATED NODE   READINESS GATES
  ingress-nginx   ingress-nginx-controller-9xhj6             1/1     Running   0               6m4s    10.233.98.130    k8s-worker-2   <none>           <none>
  ingress-nginx   ingress-nginx-controller-dcwj7             1/1     Running   0               6m4s    10.233.110.129   k8s-worker-1   <none>           <none>
  ingress-nginx   ingress-nginx-controller-lz5wm             1/1     Running   0               6m4s    10.233.104.1     k8s-master-3   <none>           <none>
  ingress-nginx   ingress-nginx-controller-ww4qs             1/1     Running   0               6m4s    10.233.91.129    k8s-master-1   <none>           <none>
  ingress-nginx   ingress-nginx-controller-z8rvp             1/1     Running   0               6m4s    10.233.118.65    k8s-master-2   <none>           <none>
  kube-system     calico-kube-controllers-68485cbf9c-hzzqj   1/1     Running   0               6m36s   10.233.98.129    k8s-worker-2   <none>           <none>
  kube-system     calico-node-6qtnz                          1/1     Running   0               7m26s   10.10.11.13      k8s-master-1   <none>           <none>
  kube-system     calico-node-cccqh                          1/1     Running   0               7m26s   10.10.10.12      k8s-master-2   <none>           <none>
  kube-system     calico-node-ctqjs                          1/1     Running   0               7m26s   10.10.11.16      k8s-worker-1   <none>           <none>
  kube-system     calico-node-hndh9                          1/1     Running   0               7m26s   10.10.11.32      k8s-master-3   <none>           <none>
  kube-system     calico-node-tgrl2                          1/1     Running   0               7m26s   10.10.11.12      k8s-worker-2   <none>           <none>
  kube-system     coredns-69db55dd76-759gw                   1/1     Running   0               5m27s   10.233.91.130    k8s-master-1   <none>           <none>
  kube-system     coredns-69db55dd76-hs6ns                   1/1     Running   0               5m18s   10.233.104.2     k8s-master-3   <none>           <none>
  kube-system     dns-autoscaler-6f4b597d8c-2ng8m            1/1     Running   0               5m20s   10.233.118.66    k8s-master-2   <none>           <none>
  kube-system     kube-apiserver-k8s-master-1                1/1     Running   1               10m     10.10.11.13      k8s-master-1   <none>           <none>
  kube-system     kube-apiserver-k8s-master-2                1/1     Running   2 (3m56s ago)   9m49s   10.10.10.12      k8s-master-2   <none>           <none>
  kube-system     kube-apiserver-k8s-master-3                1/1     Running   1               9m25s   10.10.11.32      k8s-master-3   <none>           <none>
  kube-system     kube-controller-manager-k8s-master-1       1/1     Running   2               10m     10.10.11.13      k8s-master-1   <none>           <none>
  kube-system     kube-controller-manager-k8s-master-2       1/1     Running   2               9m49s   10.10.10.12      k8s-master-2   <none>           <none>
  kube-system     kube-controller-manager-k8s-master-3       1/1     Running   2               9m22s   10.10.11.32      k8s-master-3   <none>           <none>
  kube-system     kube-proxy-7d7km                           1/1     Running   0               8m23s   10.10.11.16      k8s-worker-1   <none>           <none>
  kube-system     kube-proxy-hngxs                           1/1     Running   0               8m24s   10.10.11.13      k8s-master-1   <none>           <none>
  kube-system     kube-proxy-k5fhb                           1/1     Running   0               8m23s   10.10.11.32      k8s-master-3   <none>           <none>
  kube-system     kube-proxy-rln7x                           1/1     Running   0               8m23s   10.10.11.12      k8s-worker-2   <none>           <none>
  kube-system     kube-proxy-vzj7w                           1/1     Running   0               8m24s   10.10.10.12      k8s-master-2   <none>           <none>
  kube-system     kube-scheduler-k8s-master-1                1/1     Running   1               10m     10.10.11.13      k8s-master-1   <none>           <none>
  kube-system     kube-scheduler-k8s-master-2                1/1     Running   2 (3m46s ago)   9m49s   10.10.10.12      k8s-master-2   <none>           <none>
  kube-system     kube-scheduler-k8s-master-3                1/1     Running   1               9m25s   10.10.11.32      k8s-master-3   <none>           <none>
  kube-system     nginx-proxy-k8s-worker-1                   1/1     Running   0               8m23s   10.10.11.16      k8s-worker-1   <none>           <none>
  kube-system     nginx-proxy-k8s-worker-2                   1/1     Running   0               8m30s   10.10.11.12      k8s-worker-2   <none>           <none>
  kube-system     nodelocaldns-5ttpv                         1/1     Running   0               5m18s   10.10.11.32      k8s-master-3   <none>           <none>
  kube-system     nodelocaldns-h96ck                         1/1     Running   0               5m18s   10.10.10.12      k8s-master-2   <none>           <none>
  kube-system     nodelocaldns-l22l9                         1/1     Running   0               5m18s   10.10.11.13      k8s-master-1   <none>           <none>
  kube-system     nodelocaldns-qvdzw                         1/1     Running   0               5m18s   10.10.11.12      k8s-worker-2   <none>           <none>
  kube-system     nodelocaldns-thg98                         1/1     Running   0               5m18s   10.10.11.16      k8s-worker-1   <none>           <none>
  ```

**Кластер полностью готов к работе.**

## <a name="third-stage"></a> 5. Создание тестового приложения

### Задание

Для перехода к следующему этапу необходимо подготовить тестовое приложение, эмулирующее основное приложение разрабатываемое вашей компанией.

Способ подготовки:
1. Рекомендуемый вариант:  
   а. Создайте отдельный *git* репозиторий с простым *nginx-конфигом*, который будет отдавать статические данные.  
   б. Подготовьте *Dockerfile* для создания образа приложения.  
2. Альтернативный вариант:  
   а. Используйте любой другой код, главное, чтобы был самостоятельно создан *Dockerfile*.  

Ожидаемый результат:
1. Git-репозиторий с тестовым приложением и *Dockerfile*.
2. Регистри с собранным *docker image*. В качестве регистри может быть *DockerHub* или [*Yandex Container Registry*](https://cloud.yandex.ru/services/container-registry), созданный также с помощью *Terraform*.

---

### Результат

В качестве демо-приложения был выбран *Nginx* который отдает страничку с текстом *200 OK*. В страничке использовать CSS (для большей эстетичности).

[Репозиторий с *index.html* и *Dockerfile*](https://gitlab.com/netology-devops-project/app)

Сборка образа:
```bash
[manokhin@local:.../k8s(main)]> docker build -t manokhin/netology-demo-app:v1.0.0 .
[+] Building 1.8s (7/7) FINISHED                                                                                                                                      docker:default
 => [internal] load build definition from Dockerfile                                                                                                                            0.0s
 => => transferring dockerfile: 110B                                                                                                                                            0.0s
 => [internal] load metadata for docker.io/library/nginx:alpine                                                                                                                 1.5s
 => [internal] load .dockerignore                                                                                                                                               0.0s
 => => transferring context: 2B                                                                                                                                                 0.0s
 => [internal] load build context                                                                                                                                               0.0s
 => => transferring context: 32B                                                                                                                                                0.0s
 => CACHED [1/2] FROM docker.io/library/nginx:alpine@sha256:69f8c2c72671490607f52122be2af27d4fc09657ff57e42045801aa93d2090f7                                                    0.0s
 => [2/2] COPY index.html /usr/share/nginx/html                                                                                                                                 0.1s
 => exporting to image                                                                                                                                                          0.1s
 => => exporting layers                                                                                                                                                         0.0s
 => => writing image sha256:b583e1636c198aeefa6ce53039e8849b2d4b00d75b3fbc141994b2655de6caa7                                                                                    0.0s
 => => naming to docker.io/manokhin/netology-demo-app:v1.0.0                                                                                                                    0.0s
```

Проверка работы контейнера:
```bash
[manokhin@local:.../app(main)]> docker run -p '8080:80' -d manokhin/netology-demo-app:v1.0.0
4b130b37543431748aac4d854f6d31ae74b2018747c6871ba7e7aecc5e82a740
[manokhin@local:.../app(main)]> curl localhost:8080
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>App</title>
    <style>
        body {
            background: linear-gradient(to bottom right, #876ED7, #200772);
            font-family: 'Roboto', sans-serif;
            margin: 0;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            height: 100vh;
            color: white;
        }
        .message {
            font-size: 35vmin;
        }
        .copyright {
            font-size: 14px;
            margin-top: 50px;
        }
    </style>
</head>
<body>
    <div class="message">200 OK</div>
    <div class="copyright">manokh.in &copy; 2024</div>
</body>
</html>
```

[Образ на *Docker Hub*](https://hub.docker.com/layers/manokhin/netology-demo-app/v1.0.0/images/sha256-4f5b7d7186eaa632286831052ece33be4a4d6098ca80a0837d62cc89cd719ac8?context=explore)

**Тестовое приложение подготовлено и протестированно.**

## <a name="fourth-stage"></a> 6. Подготовка системы мониторинга и деплой приложения

### Задание

Уже должны быть готовы конфигурации для автоматического создания облачной инфраструктуры и поднятия *Kubernetes* кластера.

Теперь необходимо подготовить конфигурационные файлы для настройки нашего *Kubernetes* кластера.

Цель:
1. Задеплоить в кластер [*prometheus*](https://prometheus.io/), [*grafana*](https://grafana.com/), [*alertmanager*](https://github.com/prometheus/alertmanager), [*экспортер*](https://github.com/prometheus/node_exporter) основных метрик *Kubernetes*.
2. Задеплоить тестовое приложение, например, [*nginx*](https://www.nginx.com/) сервер отдающий статическую страницу.

Способ выполнения:
1. Воспользоваться пакетом [*kube-prometheus*](https://github.com/prometheus-operator/kube-prometheus), который уже включает в себя [*Kubernetes оператор*](https://operatorhub.io/) для [*grafana*](https://grafana.com/), [*prometheus*](https://prometheus.io/), [*alertmanager*](https://github.com/prometheus/alertmanager) и [*node_exporter*](https://github.com/prometheus/node_exporter). Альтернативный вариант - использовать набор *helm* чартов от [*bitnami*](https://github.com/bitnami/charts/tree/main/bitnami).
2. Если на первом этапе вы не воспользовались [*Terraform Cloud*](https://app.terraform.io/), то задеплойте и настройте в кластере [*atlantis*](https://www.runatlantis.io/) для отслеживания изменений инфраструктуры. Альтернативный вариант 3 задания: вместо *Terraform Cloud* или *atlantis* настройте на автоматический запуск и применение конфигурации *terraform* из вашего *git-репозитория* в выбранной вами *CI-CD* системе при любом коммите в *main* ветку. Предоставьте скриншоты работы пайплайна из *CI/CD* системы.

Ожидаемый результат:
1. Git-репозиторий с конфигурационными файлами для настройки *Kubernetes*.
2. HTTP доступ к WEB-интерфейсу *Grafana*.
3. Дашборды в *Grafana* отображающие состояние *Kubernetes-кластера*.
4. HTTP доступ к тестовому приложению.

---

### Результат

Для системы мониторинга был выбран предложенный вариант с *kube-prometheus*, т.к. он позволяет легко развернуть весь необходимый стек.

[Репозиторий с *kube-prometheus* (с созданными для контура манифестами)](https://gitlab.com/netology-devops-project/kube-prometheus/-/tree/main?ref_type=heads)

Для *Grafana* в DNS был выделен адрес http://grafana.k8s.manokh.in.

Для генерации нужных нам манифестов (которые будут содержать домен для *Grafana*, настройку *Ingress-контроллера* и *Network Polices*) требуется установить *jsonnet*, *jsonnet-bundler* и *gojsontoyaml*.

* Процесс установки и добавление в переменные окружения:
  ```bash
  [manokhin@local:.../kube-prometheus(main)]> sudo snap install go --classic
  go 1.22.4 from Canonical✓ installed
  [manokhin@local:.../kube-prometheus(main)]> go install github.com/brancz/gojsontoyaml@latest
  go: downloading github.com/brancz/gojsontoyaml v0.1.0
  go: downloading gopkg.in/yaml.v2 v2.4.0
  go: downloading github.com/ghodss/yaml v1.0.0
  [manokhin@local:.../kube-prometheus(main)]> go install github.com/google/go-jsonnet/cmd/jsonnet@latest
  go: downloading github.com/google/go-jsonnet v0.20.0
  go: downloading github.com/fatih/color v1.12.0
  go: downloading sigs.k8s.io/yaml v1.1.0
  go: downloading gopkg.in/yaml.v2 v2.2.7
  go: downloading github.com/mattn/go-isatty v0.0.12
  go: downloading github.com/mattn/go-colorable v0.1.8
  go: downloading golang.org/x/sys v0.1.0
  [manokhin@local:.../kube-prometheus(main)]> go install -a github.com/jsonnet-bundler/jsonnet-bundler/cmd/jb@latest
  go: downloading github.com/jsonnet-bundler/jsonnet-bundler v0.5.1
  go: downloading github.com/pkg/errors v0.9.1
  go: downloading github.com/fatih/color v1.13.0
  go: downloading gopkg.in/alecthomas/kingpin.v2 v2.2.6
  go: downloading github.com/mattn/go-colorable v0.1.12
  go: downloading github.com/mattn/go-isatty v0.0.14
  go: downloading github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751
  go: downloading github.com/alecthomas/units v0.0.0-20211218093645-b94a6e3cc137
  go: downloading golang.org/x/sys v0.0.0-20220615213510-4f61da869c0c
  [manokhin@local:.../kube-prometheus(main)]> export PATH=$PATH:$HOME/go/bin
  ```

* После этого требуется загрузить библиотеки и скопировать пример конфигурации:
  ```bash
  [manokhin@local:.../kube-prometheus(main)]> jb install github.com/prometheus-operator/kube-prometheus/jsonnet/kube-prometheus@main
  GET https://github.com/grafana/jsonnet-libs/archive/3637c31c4307e7bbf9c8ad7270b0798e1b840aeb.tar.gz 200
  GET https://github.com/prometheus-operator/kube-prometheus/archive/2a42427283445f19e79818223632ebf551a788d0.tar.gz 200
  LOCAL kube-prometheus -> /mnt/h/Education/netology-devops-project/kube-prometheus/jsonnet/kube-prometheus
  GET https://github.com/etcd-io/etcd/archive/debc8fb00bd935249256e397fc0e001bc2e87abb.tar.gz 200
  GET https://github.com/prometheus-operator/prometheus-operator/archive/f3091ec630c30c9bb57d9308c2c65afdf8842566.tar.gz 200
  GET https://github.com/kubernetes-monitoring/kubernetes-mixin/archive/de834e9a291b49396125768f041e2078763f48b5.tar.gz 200
  GET https://github.com/prometheus/prometheus/archive/5efc8dd27b6e68d5102b77bc708e52c9821c5101.tar.gz 200
  GET https://github.com/prometheus/alertmanager/archive/7e2cd80c2d69bb1943374c9c3bf2cda1eced73ab.tar.gz 200
  GET https://github.com/brancz/kubernetes-grafana/archive/5698c8940b6dadca3f42107b7839557bc041761f.tar.gz 200
  GET https://github.com/grafana/grafana/archive/1120f9e255760a3c104b57871fcb91801e934382.tar.gz 200
  GET https://github.com/kubernetes/kube-state-metrics/archive/89f0db6dd2fda206094b46fbcaa524731570f5b5.tar.gz 200
  GET https://github.com/prometheus/node_exporter/archive/80859a9f1879c753c2a12b3de990ad7c49579545.tar.gz 200
  GET https://github.com/pyrra-dev/pyrra/archive/551856d42dff02ec38c5b0ea6a2d99c4cb127e82.tar.gz 200
  GET https://github.com/thanos-io/thanos/archive/aa10ec301ac6e1beef8ce1fdb53ee6d9159dd904.tar.gz 200
  GET https://github.com/prometheus-operator/prometheus-operator/archive/f3091ec630c30c9bb57d9308c2c65afdf8842566.tar.gz 200
  GET https://github.com/kubernetes/kube-state-metrics/archive/89f0db6dd2fda206094b46fbcaa524731570f5b5.tar.gz 200
  GET https://github.com/grafana/grafonnet-lib/archive/a1d61cce1da59c71409b99b5c7568511fec661ea.tar.gz 200
  GET https://github.com/grafana/jsonnet-libs/archive/3637c31c4307e7bbf9c8ad7270b0798e1b840aeb.tar.gz 200
  GET https://github.com/grafana/grafonnet/archive/119d65363dff84a1976bba609f2ac3a8f450e760.tar.gz 200
  GET https://github.com/jsonnet-libs/docsonnet/archive/6ac6c69685b8c29c54515448eaca583da2d88150.tar.gz 200
  GET https://github.com/jsonnet-libs/xtd/archive/63d430b69a95741061c2f7fc9d84b1a778511d9c.tar.gz 200
  GET https://github.com/grafana/grafonnet/archive/119d65363dff84a1976bba609f2ac3a8f450e760.tar.gz 200
  GET https://github.com/grafana/grafonnet/archive/119d65363dff84a1976bba609f2ac3a8f450e760.tar.gz 200
  GET https://github.com/grafana/grafonnet-lib/archive/a1d61cce1da59c71409b99b5c7568511fec661ea.tar.gz 200
  WARN: cannot link 'github.com/prometheus-operator/kube-prometheus/jsonnet/kube-prometheus' to '/home/manokhin/netology-devops-project/kube-prometheus/vendor/kube-prometheus', because package '../jsonnet/kube-prometheus' already uses that name. The absolute import still works
  [manokhin@local:.../kube-prometheus(main)]> cp example.jsonnet monitoring.jsonne
  ```

Далее требуется добавить в конфигурацию информацию о домене *Grafana*, *Ingress-контроллере* и *сетевых политиках безопасности* (для доступа к сервису из пространства имен *Ingress-контроллера*).

[Итоговая конфигурация](https://gitlab.com/netology-devops-project/kube-prometheus/-/blob/main/monitoring.jsonnet?ref_type=heads)

* Запускаем процесс сборки:
  ```bash
  [manokhin@local:.../kube-prometheus(main)]> ./build.sh monitoring.jsonnet 
  + set -o pipefail
  ++ pwd
  + PATH=/home/manokhin/netology-devops-project/kube-prometheus/tmp/bin:/home/manokhin/.local/bin:/home/manokhin/yandex-cloud/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin:/home/manokhin/go/bin
  + rm -rf manifests
  + mkdir -p manifests/setup
  + jsonnet -J vendor -m manifests monitoring.jsonnet
  + xargs '-I{}' sh -c 'cat {} | gojsontoyaml > {}.yaml' -- '{}'
  + find manifests -type f '!' -name '*.yaml' -delete
  + rm -f kustomization
  ```

* Применение конфигурации *kube-prometheus*:
  <details>
    <summary>kubectl apply --server-side -f manifests/setup</summary>
  
    ```bash
    [manokhin@local:.../kube-prometheus(main)]> kubectl apply --server-side -f manifests/setup
    namespace/monitoring serverside-applied
    customresourcedefinition.apiextensions.k8s.io/alertmanagerconfigs.monitoring.coreos.com serverside-applied
    customresourcedefinition.apiextensions.k8s.io/alertmanagers.monitoring.coreos.com serverside-applied
    customresourcedefinition.apiextensions.k8s.io/podmonitors.monitoring.coreos.com serverside-applied
    customresourcedefinition.apiextensions.k8s.io/probes.monitoring.coreos.com serverside-applied
    customresourcedefinition.apiextensions.k8s.io/prometheuses.monitoring.coreos.com serverside-applied
    customresourcedefinition.apiextensions.k8s.io/prometheusagents.monitoring.coreos.com serverside-applied
    customresourcedefinition.apiextensions.k8s.io/prometheusrules.monitoring.coreos.com serverside-applied
    customresourcedefinition.apiextensions.k8s.io/scrapeconfigs.monitoring.coreos.com serverside-applied
    customresourcedefinition.apiextensions.k8s.io/servicemonitors.monitoring.coreos.com serverside-applied
    customresourcedefinition.apiextensions.k8s.io/thanosrulers.monitoring.coreos.com serverside-applied
    clusterrole.rbac.authorization.k8s.io/prometheus-operator serverside-applied
    clusterrolebinding.rbac.authorization.k8s.io/prometheus-operator serverside-applied
    deployment.apps/prometheus-operator serverside-applied
    networkpolicy.networking.k8s.io/prometheus-operator serverside-applied
    service/prometheus-operator serverside-applied
    serviceaccount/prometheus-operator serverside-applied
    ```
  </details>
  
  <details>
    <summary>kubectl wait --for condition=Established --all CustomResourceDefinition --namespace=monitoring</summary>
  
    ```bash
    [manokhin@local:.../kube-prometheus(main)]> kubectl wait --for condition=Established --all CustomResourceDefinition --namespace=monitoring
    customresourcedefinition.apiextensions.k8s.io/alertmanagerconfigs.monitoring.coreos.com condition met
    customresourcedefinition.apiextensions.k8s.io/alertmanagers.monitoring.coreos.com condition met
    customresourcedefinition.apiextensions.k8s.io/bgpconfigurations.crd.projectcalico.org condition met
    customresourcedefinition.apiextensions.k8s.io/bgpfilters.crd.projectcalico.org condition met
    customresourcedefinition.apiextensions.k8s.io/bgppeers.crd.projectcalico.org condition met
    customresourcedefinition.apiextensions.k8s.io/blockaffinities.crd.projectcalico.org condition met
    customresourcedefinition.apiextensions.k8s.io/caliconodestatuses.crd.projectcalico.org condition met
    customresourcedefinition.apiextensions.k8s.io/clusterinformations.crd.projectcalico.org condition met
    customresourcedefinition.apiextensions.k8s.io/felixconfigurations.crd.projectcalico.org condition met
    customresourcedefinition.apiextensions.k8s.io/globalnetworkpolicies.crd.projectcalico.org condition met
    customresourcedefinition.apiextensions.k8s.io/globalnetworksets.crd.projectcalico.org condition met
    customresourcedefinition.apiextensions.k8s.io/hostendpoints.crd.projectcalico.org condition met
    customresourcedefinition.apiextensions.k8s.io/ipamblocks.crd.projectcalico.org condition met
    customresourcedefinition.apiextensions.k8s.io/ipamconfigs.crd.projectcalico.org condition met
    customresourcedefinition.apiextensions.k8s.io/ipamhandles.crd.projectcalico.org condition met
    customresourcedefinition.apiextensions.k8s.io/ippools.crd.projectcalico.org condition met
    customresourcedefinition.apiextensions.k8s.io/ipreservations.crd.projectcalico.org condition met
    customresourcedefinition.apiextensions.k8s.io/kubecontrollersconfigurations.crd.projectcalico.org condition met
    customresourcedefinition.apiextensions.k8s.io/networkpolicies.crd.projectcalico.org condition met
    customresourcedefinition.apiextensions.k8s.io/networksets.crd.projectcalico.org condition met
    customresourcedefinition.apiextensions.k8s.io/podmonitors.monitoring.coreos.com condition met
    customresourcedefinition.apiextensions.k8s.io/probes.monitoring.coreos.com condition met
    customresourcedefinition.apiextensions.k8s.io/prometheusagents.monitoring.coreos.com condition met
    customresourcedefinition.apiextensions.k8s.io/prometheuses.monitoring.coreos.com condition met
    customresourcedefinition.apiextensions.k8s.io/prometheusrules.monitoring.coreos.com condition met
    customresourcedefinition.apiextensions.k8s.io/scrapeconfigs.monitoring.coreos.com condition met
    customresourcedefinition.apiextensions.k8s.io/servicemonitors.monitoring.coreos.com condition met
    customresourcedefinition.apiextensions.k8s.io/thanosrulers.monitoring.coreos.com condition met
    ```
  </details>
  
  <details>
    <summary>kubectl apply -f manifests/</summary>
  
    ```bash
    [manokhin@local:.../kube-prometheus(main)]> kubectl apply -f manifests/
    alertmanager.monitoring.coreos.com/main created
    networkpolicy.networking.k8s.io/alertmanager-main created
    poddisruptionbudget.policy/alertmanager-main created
    prometheusrule.monitoring.coreos.com/alertmanager-main-rules created
    secret/alertmanager-main created
    service/alertmanager-main created
    serviceaccount/alertmanager-main created
    servicemonitor.monitoring.coreos.com/alertmanager-main created
    clusterrole.rbac.authorization.k8s.io/blackbox-exporter created
    clusterrolebinding.rbac.authorization.k8s.io/blackbox-exporter created
    configmap/blackbox-exporter-configuration created
    deployment.apps/blackbox-exporter created
    networkpolicy.networking.k8s.io/blackbox-exporter created
    service/blackbox-exporter created
    serviceaccount/blackbox-exporter created
    servicemonitor.monitoring.coreos.com/blackbox-exporter created
    secret/grafana-config created
    secret/grafana-datasources created
    configmap/grafana-dashboard-alertmanager-overview created
    configmap/grafana-dashboard-apiserver created
    configmap/grafana-dashboard-cluster-total created
    configmap/grafana-dashboard-controller-manager created
    configmap/grafana-dashboard-grafana-overview created
    configmap/grafana-dashboard-k8s-resources-cluster created
    configmap/grafana-dashboard-k8s-resources-multicluster created
    configmap/grafana-dashboard-k8s-resources-namespace created
    configmap/grafana-dashboard-k8s-resources-node created
    configmap/grafana-dashboard-k8s-resources-pod created
    configmap/grafana-dashboard-k8s-resources-workload created
    configmap/grafana-dashboard-k8s-resources-workloads-namespace created
    configmap/grafana-dashboard-kubelet created
    configmap/grafana-dashboard-namespace-by-pod created
    configmap/grafana-dashboard-namespace-by-workload created
    configmap/grafana-dashboard-node-cluster-rsrc-use created
    configmap/grafana-dashboard-node-rsrc-use created
    configmap/grafana-dashboard-nodes-darwin created
    configmap/grafana-dashboard-nodes created
    configmap/grafana-dashboard-persistentvolumesusage created
    configmap/grafana-dashboard-pod-total created
    configmap/grafana-dashboard-prometheus-remote-write created
    configmap/grafana-dashboard-prometheus created
    configmap/grafana-dashboard-proxy created
    configmap/grafana-dashboard-scheduler created
    configmap/grafana-dashboard-workload-total created
    configmap/grafana-dashboards created
    deployment.apps/grafana created
    networkpolicy.networking.k8s.io/grafana created
    prometheusrule.monitoring.coreos.com/grafana-rules created
    service/grafana created
    serviceaccount/grafana created
    servicemonitor.monitoring.coreos.com/grafana created
    ingress.networking.k8s.io/grafana created
    prometheusrule.monitoring.coreos.com/kube-prometheus-rules created
    clusterrole.rbac.authorization.k8s.io/kube-state-metrics created
    clusterrolebinding.rbac.authorization.k8s.io/kube-state-metrics created
    deployment.apps/kube-state-metrics created
    networkpolicy.networking.k8s.io/kube-state-metrics created
    prometheusrule.monitoring.coreos.com/kube-state-metrics-rules created
    service/kube-state-metrics created
    serviceaccount/kube-state-metrics created
    servicemonitor.monitoring.coreos.com/kube-state-metrics created
    prometheusrule.monitoring.coreos.com/kubernetes-monitoring-rules created
    servicemonitor.monitoring.coreos.com/kube-apiserver created
    servicemonitor.monitoring.coreos.com/coredns created
    servicemonitor.monitoring.coreos.com/kube-controller-manager created
    servicemonitor.monitoring.coreos.com/kube-scheduler created
    servicemonitor.monitoring.coreos.com/kubelet created
    clusterrole.rbac.authorization.k8s.io/node-exporter created
    clusterrolebinding.rbac.authorization.k8s.io/node-exporter created
    daemonset.apps/node-exporter created
    networkpolicy.networking.k8s.io/node-exporter created
    prometheusrule.monitoring.coreos.com/node-exporter-rules created
    service/node-exporter created
    serviceaccount/node-exporter created
    servicemonitor.monitoring.coreos.com/node-exporter created
    apiservice.apiregistration.k8s.io/v1beta1.metrics.k8s.io created
    clusterrole.rbac.authorization.k8s.io/prometheus-adapter created
    clusterrole.rbac.authorization.k8s.io/system:aggregated-metrics-reader created
    clusterrolebinding.rbac.authorization.k8s.io/prometheus-adapter created
    clusterrolebinding.rbac.authorization.k8s.io/resource-metrics:system:auth-delegator created
    clusterrole.rbac.authorization.k8s.io/resource-metrics-server-resources created
    configmap/adapter-config created
    deployment.apps/prometheus-adapter created
    networkpolicy.networking.k8s.io/prometheus-adapter created
    poddisruptionbudget.policy/prometheus-adapter created
    rolebinding.rbac.authorization.k8s.io/resource-metrics-auth-reader created
    service/prometheus-adapter created
    serviceaccount/prometheus-adapter created
    servicemonitor.monitoring.coreos.com/prometheus-adapter created
    clusterrole.rbac.authorization.k8s.io/prometheus-k8s created
    clusterrolebinding.rbac.authorization.k8s.io/prometheus-k8s created
    networkpolicy.networking.k8s.io/prometheus-k8s created
    prometheusrule.monitoring.coreos.com/prometheus-operator-rules created
    servicemonitor.monitoring.coreos.com/prometheus-operator created
    poddisruptionbudget.policy/prometheus-k8s created
    prometheus.monitoring.coreos.com/k8s created
    prometheusrule.monitoring.coreos.com/prometheus-k8s-prometheus-rules created
    rolebinding.rbac.authorization.k8s.io/prometheus-k8s-config created
    rolebinding.rbac.authorization.k8s.io/prometheus-k8s created
    rolebinding.rbac.authorization.k8s.io/prometheus-k8s created
    rolebinding.rbac.authorization.k8s.io/prometheus-k8s created
    role.rbac.authorization.k8s.io/prometheus-k8s-config created
    role.rbac.authorization.k8s.io/prometheus-k8s created
    role.rbac.authorization.k8s.io/prometheus-k8s created
    role.rbac.authorization.k8s.io/prometheus-k8s created
    service/prometheus-k8s created
    serviceaccount/prometheus-k8s created
    servicemonitor.monitoring.coreos.com/prometheus-k8s created
    ```
  </details>

Проверка состояния подов:
```bash
[manokhin@local:~()]> kubectl get services -n monitoring
NAME                    TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)                      AGE
alertmanager-main       ClusterIP   10.233.27.130   <none>        9093/TCP,8080/TCP            12h
alertmanager-operated   ClusterIP   None            <none>        9093/TCP,9094/TCP,9094/UDP   12h
blackbox-exporter       ClusterIP   10.233.5.4      <none>        9115/TCP,19115/TCP           12h
grafana                 ClusterIP   10.233.62.131   <none>        3000/TCP                     12h
kube-state-metrics      ClusterIP   None            <none>        8443/TCP,9443/TCP            12h
node-exporter           ClusterIP   None            <none>        9100/TCP                     12h
prometheus-adapter      ClusterIP   10.233.55.48    <none>        443/TCP                      12h
prometheus-k8s          ClusterIP   10.233.39.48    <none>        9090/TCP,8080/TCP            12h
prometheus-operated     ClusterIP   None            <none>        9090/TCP                     12h
prometheus-operator     ClusterIP   None            <none>        8443/TCP                     12h
```

Скриншот *Grafana* с доступом по домену:
![](./assets/images/grafana_dashboard.png)

---

Для демо приложения в DNS был выделен адрес https://app.k8s.manokh.in.

Добавим поддержку SSL-сертификатов *Let's Encrypt*.

Установка *cert-manager*:
```bash
[manokhin@local:.../netology-devops-project()]> kubectl apply -f https://github.com/cert-manager/cert-manager/releases/download/v1.15.0/cert-manager.yaml
namespace/cert-manager created
customresourcedefinition.apiextensions.k8s.io/certificaterequests.cert-manager.io created
customresourcedefinition.apiextensions.k8s.io/certificates.cert-manager.io created
customresourcedefinition.apiextensions.k8s.io/challenges.acme.cert-manager.io created
customresourcedefinition.apiextensions.k8s.io/clusterissuers.cert-manager.io created
customresourcedefinition.apiextensions.k8s.io/issuers.cert-manager.io created
customresourcedefinition.apiextensions.k8s.io/orders.acme.cert-manager.io created
serviceaccount/cert-manager-cainjector created
serviceaccount/cert-manager created
serviceaccount/cert-manager-webhook created
clusterrole.rbac.authorization.k8s.io/cert-manager-cainjector created
clusterrole.rbac.authorization.k8s.io/cert-manager-controller-issuers created
clusterrole.rbac.authorization.k8s.io/cert-manager-controller-clusterissuers created
clusterrole.rbac.authorization.k8s.io/cert-manager-controller-certificates created
clusterrole.rbac.authorization.k8s.io/cert-manager-controller-orders created
clusterrole.rbac.authorization.k8s.io/cert-manager-controller-challenges created
clusterrole.rbac.authorization.k8s.io/cert-manager-controller-ingress-shim created
clusterrole.rbac.authorization.k8s.io/cert-manager-cluster-view created
clusterrole.rbac.authorization.k8s.io/cert-manager-view created
clusterrole.rbac.authorization.k8s.io/cert-manager-edit created
clusterrole.rbac.authorization.k8s.io/cert-manager-controller-approve:cert-manager-io created
clusterrole.rbac.authorization.k8s.io/cert-manager-controller-certificatesigningrequests created
clusterrole.rbac.authorization.k8s.io/cert-manager-webhook:subjectaccessreviews created
clusterrolebinding.rbac.authorization.k8s.io/cert-manager-cainjector created
clusterrolebinding.rbac.authorization.k8s.io/cert-manager-controller-issuers created
clusterrolebinding.rbac.authorization.k8s.io/cert-manager-controller-clusterissuers created
clusterrolebinding.rbac.authorization.k8s.io/cert-manager-controller-certificates created
clusterrolebinding.rbac.authorization.k8s.io/cert-manager-controller-orders created
clusterrolebinding.rbac.authorization.k8s.io/cert-manager-controller-challenges created
clusterrolebinding.rbac.authorization.k8s.io/cert-manager-controller-ingress-shim created
clusterrolebinding.rbac.authorization.k8s.io/cert-manager-controller-approve:cert-manager-io created
clusterrolebinding.rbac.authorization.k8s.io/cert-manager-controller-certificatesigningrequests created
clusterrolebinding.rbac.authorization.k8s.io/cert-manager-webhook:subjectaccessreviews created
role.rbac.authorization.k8s.io/cert-manager-cainjector:leaderelection created
role.rbac.authorization.k8s.io/cert-manager:leaderelection created
role.rbac.authorization.k8s.io/cert-manager-webhook:dynamic-serving created
rolebinding.rbac.authorization.k8s.io/cert-manager-cainjector:leaderelection created
rolebinding.rbac.authorization.k8s.io/cert-manager:leaderelection created
rolebinding.rbac.authorization.k8s.io/cert-manager-webhook:dynamic-serving created
service/cert-manager created
service/cert-manager-webhook created
deployment.apps/cert-manager-cainjector created
deployment.apps/cert-manager created
deployment.apps/cert-manager-webhook created
mutatingwebhookconfiguration.admissionregistration.k8s.io/cert-manager-webhook created
validatingwebhookconfiguration.admissionregistration.k8s.io/cert-manager-webhook created
```

Создадим *ClusterIssuer* для *Let's Encrypt*:
  * Конфигурация:
    ```yaml
    ---
    apiVersion: cert-manager.io/v1
    kind: ClusterIssuer
    metadata:
      name: letsencrypt-k8s
    spec:
      acme:
        server: https://acme-v02.api.letsencrypt.org/directory
        email: ***
        privateKeySecretRef:
          name: letsencrypt-k8s
        solvers:
        - http01:
            ingress:
              class: nginx
    ```
  * Применение:
    ```bash
    [manokhin@local:.../netology-devops-project()]> kubectl apply -f letsencrypt-clusterissuer.yaml
    clusterissuer.cert-manager.io/letsencrypt-prod created
    ```

Подготовленные *Namespace*, *Deployment*, *Service*, *Ingress*, *NetworkPolicy* манифесты для приложения:
  * [namespace.yml](https://gitlab.com/netology-devops-project/app/-/blob/main/k8s/namespace.yml?ref_type=heads)
  * [deployment.yml](https://gitlab.com/netology-devops-project/app/-/blob/main/k8s/deployment.yml?ref_type=heads)
  * [service.yml](https://gitlab.com/netology-devops-project/app/-/blob/main/k8s/service.yml?ref_type=heads)
  * [ingress.yml](https://gitlab.com/netology-devops-project/app/-/blob/main/k8s/ingress.yml?ref_type=heads)
  * [networkpolicy.yml](https://gitlab.com/netology-devops-project/app/-/blob/main/k8s/networkpolicy.yml?ref_type=heads)

Применение конфигураций:
```bash
[manokhin@local:.../k8s(main)]> kubectl apply -f namespace.yml
namespace/apps created
[manokhin@local:.../k8s(main)]> kubectl apply -f deployment.yml
deployment.apps/demo-app created
[manokhin@local:.../k8s(main)]> kubectl apply -f service.yml
service/demo-app-svc created
[manokhin@local:.../k8s(main)]> kubectl apply -f ingress.yml
ingress.networking.k8s.io/demo-app-ingress created
[manokhin@local:.../k8s(main)]> kubectl apply -f networkpolicy.yml
networkpolicy.networking.k8s.io/allow-ingress-namespace created
```

Проверка состояния пода и сервиса:
```bash
[manokhin@local:.../k8s(main)]> kubectl get pods -n apps
NAME                        READY   STATUS    RESTARTS   AGE
demo-app-74cb697df8-mr7bw   1/1     Running   0          13m
[manokhin@local:.../k8s(main)]> kubectl get services -n apps
NAME           TYPE        CLUSTER-IP     EXTERNAL-IP   PORT(S)   AGE
demo-app-svc   ClusterIP   10.233.13.14   <none>        80/TCP    13m
```

Скриншот задеплоенного приложения с сертификатом в кластере *K8s*:
![](./assets/images/k8s_demo_app_with_cert.png)

**Кластер замониторен, тестовое приложение развернуто и доступно.**

## <a name="fifth-stage"></a> 7. Установка и настройка CI/CD

### Задание

Осталось настроить *CI/CD* систему для автоматической сборки *docker image* и деплоя приложения при изменении кода.

Цель:
1. Автоматическая сборка *Docker* образа при коммите в репозиторий с тестовым приложением.
2. Автоматический деплой нового *Docker* образа.

Можно использовать [*Teamcity*](https://www.jetbrains.com/ru-ru/teamcity/), [*Jenkins*](https://www.jenkins.io/), [*GitLab CI*](https://about.gitlab.com/stages-devops-lifecycle/continuous-integration/) или *GitHub Actions*.

Ожидаемый результат:
1. Интерфейс *CI/CD* сервиса доступен по *HTTP*.
2. При любом коммите в репозиторий с тестовым приложением происходит сборка и отправка в регистр *Docker* образа.
3. При создании тега (например *v1.0.0*) происходит сборка и отправка с соответствующим *label* в регистри, а также деплой соответствующего *Docker* образа в кластер *Kubernetes*.

---

### Результат

При планировании работы для *CI/CD* системы был выбран *GitLab*.

Для начала подготовим необходимые переменные (данные для доступа к *Docker Hub* и *Kubernetes*) в *GitLab*:
![](./assets/images/gitlab_ci_vars.png)

*CI/CD* будет разделен на четыре этапа:
  1. Сборка и тегирование образа на основе хэша коммита (а также создание артефакта).
  2. Тестирование образа (*curl*), выполняется только при коммите (*only branches*).
  3. Пуш образа на *(Docker Hub)* (если использовался тег в формате *"v*"*, то образ будет дополнительно протегирован и загружен на *Docker Hub* с лейблом).
  4. Деплой образа на *Kubernetes-кластер*, выполняется только при добавлении тега (*only tags*). Так же на этом этапе происходит [замена](https://gitlab.com/netology-devops-project/app/-/blob/main/.gitlab-ci.yml?ref_type=heads#L72) переменных в файле [*k8s/deployment.yml*](https://gitlab.com/netology-devops-project/app/-/blob/main/k8s/deployment.yml?ref_type=heads#L19) на текущий лейбл.

Для первых трех этапов используется образ *docker:latest*, для деплоя в *Kubernetes* используется образ *bitnami/kubectl:latest*. 


[Итоговая конфигурация *CI/CD*](https://gitlab.com/netology-devops-project/app/-/blob/main/.gitlab-ci.yml?ref_type=heads)

Отработка *CI* при коммите:
  * [Пайплайн](https://gitlab.com/netology-devops-project/app/-/pipelines/1340623110/builds)
  * [Полученный *Docker-образ*](https://hub.docker.com/layers/manokhin/netology-demo-app/0faef1d1534c3febb5b4a7c41df60e4522cd3d7d/images/sha256-2c3793f06da4844f01158ae76b2f5f38b72c9c118ba9b94cb519bcf090fcfd16?context=explore)

Отработка *CD* при тегировании:
  * [Тег](https://gitlab.com/netology-devops-project/app/-/tags/v1.0.4)
  * [Пайплайн](https://gitlab.com/netology-devops-project/app/-/pipelines/1340628337/builds)
  * [Полученный *Docker-образ* с лейблом](https://hub.docker.com/layers/manokhin/netology-demo-app/v1.0.4/images/sha256-2c3793f06da4844f01158ae76b2f5f38b72c9c118ba9b94cb519bcf090fcfd16?context=explore)
  * Проверка в *Kubernetes-кластере* (обновление версии):
    ```bash
    [manokhin@local:.../app(main)]> kubectl get pods -n apps
    NAME                        READY   STATUS    RESTARTS   AGE
    demo-app-7f7cd4d4c8-4z2sg   1/1     Running   0          2m21s
    [manokhin@local:.../app(main)]> kubectl describe pods demo-app-7f7cd4d4c8-4z2sg -n apps | grep "Image:"
        Image:          manokhin/netology-demo-app:v1.0.4
    ```

**Реализация CI/CD-процессов для демо-приложения выполнена.**

## <a name="conclusions"></a> 8. Заключение

Проект был направлен на создание и настройку облачной инфраструктуры на платформе *Яндекс.Облако*, развертывание *Kubernetes-кластера*, настройку системы мониторинга, а также автоматизацию сборки, тестирования, и развёртывания приложения с использованием *Docker-контейнеров*.

Выполнение проекта позволило успешно развернуть и интегрировать ключевые инструменты и сервисы для работы с облачной инфраструктурой и *Kubernetes*, а также закрепить полученные на курсе навыки. Автоматизация с помощью *Terraform* и *CI/CD* значительно упростила управление инфраструктурой и разработкой приложений, обеспечивая стабильность и масштабируемость процессов разработки и развёртывания.
